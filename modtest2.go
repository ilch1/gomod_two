package gomod_two

import "fmt"
import "gitlab.com/ilch1/gomod_one"

func Modtest2() {
	gomod_one.Modtest1()
	fmt.Printf("gomod_two, version 1.0.0\n")
}
